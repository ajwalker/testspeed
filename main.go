package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"text/tabwriter"
	"time"

	"github.com/xanzy/go-gitlab"
)

var matchLine = regexp.MustCompile(`^--- PASS: (\w+) \((.*)\)`)

var (
	projectArg  string
	tokenArg    string
	moreThanArg time.Duration
	snippetArg  string
)

func init() {
	flag.StringVar(&projectArg, "project", "gitlab-org/gitlab-runner", "gitlab project")
	flag.StringVar(&tokenArg, "token", "", "token token")
	flag.DurationVar(&moreThanArg, "morethan", time.Duration(0), "more than duration")
	flag.StringVar(&snippetArg, "snippet", "", "snippet name to create snippet")
}

type Processor struct {
	client    *gitlab.Client
	results   map[string]map[string]map[int]*Result
	pipelines []int

	resultsLock sync.Mutex
}

type Result struct {
	Duration time.Duration
}

type ResultOptions struct {
	MoreThan  time.Duration
	Highlight bool
}

func NewProcessor(client *gitlab.Client) *Processor {
	return &Processor{
		client:  client,
		results: make(map[string]map[string]map[int]*Result),
	}
}

func (p *Processor) Results(options ResultOptions) [][]string {
	// sort by test names
	names := make([]string, 0, len(p.results))
	for name := range p.results {
		names = append(names, name)
	}
	sort.Strings(names)

	rows := make([][]string, 0, len(p.results))
	for _, name := range names {
		categories := p.results[name]
		include := false

		high := time.Duration(0)
		low := time.Duration(math.MaxInt64)
		for _, results := range categories {
			for _, result := range results {
				if result.Duration > options.MoreThan {
					include = true
				}
				if result.Duration > high {
					high = result.Duration
				}
				if result.Duration < low {
					low = result.Duration
				}
			}
		}

		if !include {
			continue
		}

		for categoryName, results := range categories {
			cols := make([]string, 0, 2+len(p.pipelines))
			cols = append(cols, name)
			cols = append(cols, categoryName)
			for _, pid := range p.pipelines {
				result, ok := results[pid]
				text := "-"
				if ok {
					prefix := ""
					suffix := ""
					if options.Highlight && low != high {
						if result.Duration == low {
							prefix = "[+ "
							suffix = " +]"
						}
						if result.Duration == high {
							prefix = "[- "
							suffix = " -]"
						}
					}
					text = prefix + result.Duration.String() + suffix
				}
				cols = append(cols, text)
			}
			rows = append(rows, cols)
		}
	}

	return rows
}

func (p *Processor) ProcessPipeline(projectID string, pipelineID int) error {
	opts := &gitlab.ListJobsOptions{}
	opts.PerPage = 50

	jobs, _, err := p.client.Jobs.ListPipelineJobs(projectID, pipelineID, opts)
	if err != nil {
		return err
	}

	p.resultsLock.Lock()
	p.pipelines = append(p.pipelines, pipelineID)
	p.resultsLock.Unlock()

	for _, job := range jobs {
		if job.Stage != "test" {
			continue
		}

		r, _, err := p.client.Jobs.GetTraceFile(projectID, job.ID)
		if err != nil {
			return err
		}

		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			matches := matchLine.FindAllStringSubmatch(scanner.Text(), -1)
			if len(matches) == 0 {
				continue
			}

			duration, err := time.ParseDuration(matches[0][2])
			if err != nil {
				return err
			}

			testName := matches[0][1]

			p.resultsLock.Lock()

			testEntry, ok := p.results[testName]
			if !ok {
				testEntry = make(map[string]map[int]*Result)
			}
			resultEntry, ok := testEntry[job.Name]
			if !ok {
				resultEntry = make(map[int]*Result)
			}
			resultEntry[pipelineID] = &Result{Duration: duration}
			testEntry[job.Name] = resultEntry
			p.results[testName] = testEntry

			p.resultsLock.Unlock()
		}
	}
	return nil
}

func main() {
	flag.Parse()

	git, err := gitlab.NewClient(tokenArg)
	if err != nil {
		panic(err)
	}

	var wg sync.WaitGroup
	processor := NewProcessor(git)

	pipelineIDs := flag.Args()
	for _, arg := range pipelineIDs {
		pipelineID, err := strconv.Atoi(arg)
		if err != nil {
			panic(err)
		}

		wg.Add(1)
		go func() {
			defer wg.Done()
			if err := processor.ProcessPipeline(projectArg, pipelineID); err != nil {
				panic(err)
			}
		}()
	}
	wg.Wait()

	buf := new(bytes.Buffer)

	w := tabwriter.NewWriter(buf, 0, 0, 3, ' ', tabwriter.Debug)

	divider := "----\t----\t" + strings.Repeat("----\t", len(pipelineIDs))

	fmt.Fprintln(w, "Test\tCategory\t"+strings.Join(pipelineIDs, "\t"))
	fmt.Fprintln(w, divider)

	rows := processor.Results(ResultOptions{
		Highlight: true,
		MoreThan:  moreThanArg,
	})
	last := ""
	for i, cols := range rows {
		if i == 0 {
			last = cols[0]
		}
		if i > 0 && cols[0] != last {
			fmt.Fprintln(w, divider)
			last = cols[0]
		}
		fmt.Fprintln(w, strings.Join(cols, "\t"))
	}

	if err := w.Flush(); err != nil {
		panic(err)
	}

	content := buf.Bytes()
	os.Stdout.Write(content)

	if len(snippetArg) > 0 {
		snippet, _, err := git.Snippets.CreateSnippet(&gitlab.CreateSnippetOptions{
			Title:       gitlab.String(snippetArg),
			FileName:    gitlab.String("testspeed.md"),
			Description: gitlab.String("using morethan=" + moreThanArg.String()),
			Content:     gitlab.String(string(content)),
			Visibility:  gitlab.Visibility(gitlab.PrivateVisibility),
		})
		if err != nil {
			panic(err)
		}
		fmt.Printf("\n\nSnippet created @ %s\n\n", snippet.WebURL)
	}
}
