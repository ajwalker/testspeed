# testspeed

```
Usage of ./testspeed:
  -morethan duration
    	more than duration
  -project string
    	gitlab project (default "gitlab-org/gitlab-runner")
  -snippet string
    	snippet name to create snippet
  -token string
    	token token
```

```
# output pipeline go test times
./testspeed --token "my-super-secret-token" 146178860

# compare pipeline go test times
$ ./testspeed --token "my-super-secret-token" 146178860 146130404

# only include a test if any of the durations were above 5s
$ ./testspeed --token "my-super-secret-token" --morethan 5s 146178860 146130404

# create a snippet automatically
$ ./testspeed --token "my-super-secret-token" -snippet "Docker Wait Speeds" 146178860 146130404

...

Snippet created @ https://gitlab.com/snippets/1976760
```

